# -*- coding: utf-8  -*-

from django.forms import inlineformset_factory
from django import forms
from django.contrib.auth.models import User
from django.utils import timezone

from cadastro.models import (
	Equipamento,
	Propriedade,
	Insumo,
	Produto,
	MaoDeObra,
	ClienteFornecedor,
	)


class ProdutoForm(forms.ModelForm):

	def __init__(self, *args, **kwargs):
		self.request_user = kwargs.pop('request_user')
		super(ProdutoForm, self).__init__(*args, **kwargs)

	class Meta:
		model = Produto
		fields = '__all__'
		exclude = ['user','propriedade']
		widgets = {
			'produto': forms.TextInput(attrs={'class': 'form-control'}),
			'tipo': forms.TextInput(attrs={'class': 'form-control'}),
			'variedade': forms.TextInput(attrs={'class': 'form-control'}),
			'ciclo_viveiro': forms.NumberInput(attrs={'class': 'form-control'}),
			'ciclo_campo': forms.NumberInput(attrs={'class': 'form-control'}),
			'celulas_bandeja': forms.NumberInput(attrs={'class': 'form-control'}),
			'densidade': forms.NumberInput(attrs={'class': 'form-control'}),
			'tipo_plantio': forms.Select(attrs={'class': 'form-control'}),
		}

	def save(self, commit=True):
		instance = super(ProdutoForm, self).save(commit=False)
		
		instance.user = self.request_user
		instance.data_cadastro = timezone.now()
		instance.propriedade = self.request_user.perfilusuario.propriedade

		if commit:
			instance.save()
			self.save_m2m()
		return instance


class EquipamentoForm(forms.ModelForm):

	def __init__(self, *args, **kwargs):
		self.request_user = kwargs.pop('request_user')
		super(EquipamentoForm, self).__init__(*args, **kwargs)

	class Meta:
		model = Equipamento
		fields = '__all__'
		exclude = ['user','propriedade']
		widgets = {
			'equipamento': forms.Select(attrs={'class': 'form-control'}),
			'fabricante': forms.TextInput(attrs={'class': 'form-control'}),
			'valor_inicial': forms.TextInput(attrs={'class': 'form-control'}),
			'depreciacao': forms.NumberInput(attrs={'class': 'form-control'}),
			'manutencao': forms.NumberInput(attrs={'class': 'form-control'}),
			'custo_hora': forms.NumberInput(attrs={'class': 'form-control'}),
			'ano': forms.NumberInput(attrs={'class': 'form-control'}),
		}

	def save(self, commit=True):
		instance = super(EquipamentoForm, self).save(commit=False)
		
		instance.user = self.request_user
		instance.data_cadastro = timezone.now()
		instance.propriedade = self.request_user.perfilusuario.propriedade

		if commit:
			instance.save()
			self.save_m2m()
		return instance


class InsumoForm(forms.ModelForm):

	def __init__(self, *args, **kwargs):
		self.request_user = kwargs.pop('request_user')
		super(InsumoForm, self).__init__(*args, **kwargs)

	class Meta:
		model = Insumo
		fields = '__all__'
		exclude = ['user','propriedade']
		widgets = {
			'insumo': forms.Select(attrs={'class': 'form-control'}),
			'fabricante': forms.TextInput(attrs={'class': 'form-control'}),
			'data_compra': forms.DateInput(attrs={'class': 'date-picker form-control'}),
			'data_validade': forms.DateInput(attrs={'class': 'date-picker form-control'}),
			'preco_unidade': forms.NumberInput(attrs={'class': 'form-control'}),
			'quantidade': forms.NumberInput(attrs={'class': 'form-control'}),
			'valor_total': forms.NumberInput(attrs={'class': 'form-control'}),
		}

	def save(self, commit=True):
		instance = super(InsumoForm, self).save(commit=False)
		
		instance.user = self.request_user
		instance.data_cadastro = timezone.now()
		instance.propriedade = self.request_user.perfilusuario.propriedade

		if commit:
			instance.save()
			self.save_m2m()
		return instance


class MaoDeObraForm(forms.ModelForm):

	def __init__(self, *args, **kwargs):
		self.request_user = kwargs.pop('request_user')
		super(MaoDeObraForm, self).__init__(*args, **kwargs)

	class Meta:
		model = MaoDeObra
		fields = '__all__'
		exclude = ['user','propriedade']
		widgets = {
			'nome': forms.TextInput(attrs={'class': 'form-control'}),
			'matricula': forms.TextInput(attrs={'class': 'form-control'}),
			'funcao': forms.TextInput(attrs={'class': 'form-control'}),
			'nome': forms.TextInput(attrs={'class': 'form-control'}),
			'regime_contratacao': forms.Select(attrs={'class': 'form-control'}),
			'data_admissao': forms.DateInput(attrs={'class': 'date-picker form-control'}),
			'carga_horaria': forms.NumberInput(attrs={'class': 'form-control'}),
			'salario': forms.NumberInput(attrs={'class': 'form-control'}),
			'encargos_trabalhistas': forms.NumberInput(attrs={'class': 'form-control'}),
			'beneficios': forms.NumberInput(attrs={'class': 'form-control'}),
			'custo_hora': forms.NumberInput(attrs={'class': 'form-control'}),
		}

	def save(self, commit=True):
		instance = super(MaoDeObraForm, self).save(commit=False)
		
		instance.user = self.request_user
		instance.data_cadastro = timezone.now()
		instance.propriedade = self.request_user.perfilusuario.propriedade

		if commit:
			instance.save()
			self.save_m2m()
		return instance

class ClienteFornecedorForm(forms.ModelForm):

	def __init__(self, *args, **kwargs):
		self.request_user = kwargs.pop('request_user')
		super(ClienteFornecedorForm, self).__init__(*args, **kwargs)

	class Meta:
		model = ClienteFornecedor
		fields = '__all__'
		exclude = ['user','propriedade']
		widgets = {
			'tipo': forms.Select(attrs={'class': 'form-control'}),
			'tipo_pessoa': forms.Select(attrs={'class': 'form-control'}),
			'razao_social': forms.TextInput(attrs={'class': 'form-control'}),
			'nome_fantasia': forms.TextInput(attrs={'class': 'form-control'}),
		}

	def save(self, commit=True):
		instance = super(ClienteFornecedorForm, self).save(commit=False)
		
		instance.user = self.request_user
		instance.data_cadastro = timezone.now()
		instance.propriedade = self.request_user.perfilusuario.propriedade

		if commit:
			instance.save()
			self.save_m2m()
		return instance