# coding: utf-8

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ImproperlyConfigured
from django.core.urlresolvers import reverse_lazy, reverse
from django.forms import models as model_forms, ModelChoiceField
from django.forms.formsets import formset_factory
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.shortcuts import redirect, render, get_object_or_404, render_to_response
from django.contrib.admin.views.decorators import staff_member_required
from django.apps import apps

from django.contrib.auth.models import User

from django.utils import timezone
import datetime

from django.conf import settings

from cadastro.models import (
	PerfilUsuario,
	Equipamento,
	Propriedade,
	Insumo,
	Produto,
	MaoDeObra,
	ClienteFornecedor,
	)

from .forms import (
	ProdutoForm,
	EquipamentoForm,
	InsumoForm,
	MaoDeObraForm,
	ClienteFornecedorForm,
	)

@login_required
def produto_cadastrar(request):

	form = ProdutoForm(
		data=request.POST or None,
		request_user=request.user,
	)

	if request.POST:
		
		if form.is_valid():

			f = form.save()

			messages.success(request, u'Produto cadastrado com sucesso')
			return HttpResponseRedirect(reverse_lazy('produto_listar'))

	context = {
		'form': form,
	}

	return render(request, 'cadastro/produto_cadastrar.html', context)


@login_required
def produto_listar(request):

	produto = Produto.objects.get_queryset_by_user(request.user).order_by('produto')

	context = {
		'produto': produto,
	}

	return render(request, 'cadastro/produto_listar.html', context)

@login_required
def produto_remover(request, id):

	lote = Produto.objects.get(pk=int(id))

	lote.delete()

	messages.success(request, u'Produto removido com sucesso')
	return HttpResponseRedirect(reverse_lazy('produto_listar'))

@login_required
def produto_editar(request, id):

	lote = Produto.objects.get(pk=int(id))

	form = ProdutoForm(request.POST or None, instance=lote, request_user=request.user)

	if request.method == 'POST':
		
		if form.is_valid():

			f = form.save()

			messages.success(request, u'Produto editado com sucesso')
			return HttpResponseRedirect(reverse_lazy('produto_listar'))

	context = {
		'form': form,
	}

	return render(request, 'cadastro/produto_editar.html', context)


@login_required
def equipamento_cadastrar(request):

	form = EquipamentoForm(
		data=request.POST or None,
		request_user=request.user,
	)

	if request.POST:
		
		if form.is_valid():

			f = form.save()

			messages.success(request, u'Equipamento cadastrado com sucesso')
			return HttpResponseRedirect(reverse_lazy('equipamento_listar'))

	context = {
		'form': form,
	}

	return render(request, 'cadastro/equipamento_cadastrar.html', context)


@login_required
def equipamento_listar(request):

	equipamento = Equipamento.objects.get_queryset_by_user(request.user).order_by('equipamento')

	context = {
		'equipamento': equipamento,
	}

	return render(request, 'cadastro/equipamento_listar.html', context)

@login_required
def equipamento_remover(request, id):

	lote = Equipamento.objects.get(pk=int(id))

	lote.delete()

	messages.success(request, u'Equipamento removido com sucesso')
	return HttpResponseRedirect(reverse_lazy('equipamento_listar'))

@login_required
def equipamento_editar(request, id):

	lote = Equipamento.objects.get(pk=int(id))

	form = EquipamentoForm(request.POST or None, instance=lote, request_user=request.user)

	if request.method == 'POST':
		
		if form.is_valid():

			f = form.save()

			messages.success(request, u'Equipamento editado com sucesso')
			return HttpResponseRedirect(reverse_lazy('equipamento_listar'))

	context = {
		'form': form,
	}

	return render(request, 'cadastro/equipamento_editar.html', context)


@login_required
def insumo_cadastrar(request):

	form = InsumoForm(
		data=request.POST or None,
		request_user=request.user,
	)

	if request.POST:
		
		if form.is_valid():

			f = form.save()

			messages.success(request, u'Insumo cadastrado com sucesso')
			return HttpResponseRedirect(reverse_lazy('insumo_listar'))

	context = {
		'form': form,
	}

	return render(request, 'cadastro/insumo_cadastrar.html', context)


@login_required
def insumo_listar(request):

	insumo = Insumo.objects.get_queryset_by_user(request.user).order_by('insumo')

	context = {
		'insumo': insumo,
	}

	return render(request, 'cadastro/insumo_listar.html', context)

@login_required
def insumo_remover(request, id):

	lote = Insumo.objects.get(pk=int(id))

	lote.delete()

	messages.success(request, u'Insumo removido com sucesso')
	return HttpResponseRedirect(reverse_lazy('insumo_listar'))

@login_required
def insumo_editar(request, id):

	lote = Insumo.objects.get(pk=int(id))

	form = InsumoForm(request.POST or None, instance=lote, request_user=request.user)

	if request.method == 'POST':
		
		if form.is_valid():

			f = form.save()

			messages.success(request, u'Insumo editado com sucesso')
			return HttpResponseRedirect(reverse_lazy('insumo_listar'))

	context = {
		'form': form,
	}

	return render(request, 'cadastro/insumo_editar.html', context)


@login_required
def mdo_cadastrar(request):

	form = MaoDeObraForm(
		data=request.POST or None,
		request_user=request.user,
	)

	if request.POST:
		
		if form.is_valid():

			f = form.save()

			messages.success(request, u'Cadastrado com sucesso')
			return HttpResponseRedirect(reverse_lazy('mdo_listar'))

	context = {
		'form': form,
	}

	return render(request, 'cadastro/mdo_cadastrar.html', context)


@login_required
def mdo_listar(request):

	mdo = MaoDeObra.objects.get_queryset_by_user(request.user).order_by('nome')

	context = {
		'mdo': mdo,
	}

	return render(request, 'cadastro/mdo_listar.html', context)

@login_required
def mdo_remover(request, id):

	lote = MaoDeObra.objects.get(pk=int(id))

	lote.delete()

	messages.success(request, u'Removido com sucesso')
	return HttpResponseRedirect(reverse_lazy('mdo_listar'))

@login_required
def mdo_editar(request, id):

	lote = MaoDeObra.objects.get(pk=int(id))

	form = MaoDeObraForm(request.POST or None, instance=lote, request_user=request.user)

	if request.method == 'POST':
		
		if form.is_valid():

			f = form.save()

			messages.success(request, u'Editado com sucesso')
			return HttpResponseRedirect(reverse_lazy('mdo_listar'))

	context = {
		'form': form,
	}

	return render(request, 'cadastro/mdo_editar.html', context)



@login_required
def clientefornecedor_cadastrar(request):

	form = ClienteFornecedorForm(
		data=request.POST or None,
		request_user=request.user,
	)

	if request.POST:
		
		if form.is_valid():

			f = form.save()

			messages.success(request, u'Cliente/Fornecedor cadastrado com sucesso')
			return HttpResponseRedirect(reverse_lazy('clientefornecedor_listar'))

	context = {
		'form': form,
	}

	return render(request, 'cadastro/clientefornecedor_cadastrar.html', context)


@login_required
def clientefornecedor_listar(request):

	clientefornecedor = ClienteFornecedor.objects.get_queryset_by_user(request.user).order_by('razao_social')

	context = {
		'clientefornecedor': clientefornecedor,
	}

	return render(request, 'cadastro/clientefornecedor_listar.html', context)