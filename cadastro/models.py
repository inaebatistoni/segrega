# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.gis.db import models
from django.contrib.auth.models import User
from django import utils
from django.utils import timezone

TIPO_CHOICES = (
    ('1', 'Cliente'),
    ('2', 'Fornecedor'),
)

UNIDADE_CHOICES = (
    ('1', 'Kg'),
    ('2', 'Lt'),
)

TIPO_PESSOA_CHOICES = (
    ('1', 'Pessoa Física'),
    ('2', 'Pessoa Jurídica'),
)

TIPO_INSUMO_CHOICES = (
    ('1', 'Adubo'),
    ('2', 'Agrotóxico'),
)

TIPO_PLANTIO_CHOICES = (
    ('1', 'Muda'),
    ('2', 'Semente'),
)


class PerfilUsuario(models.Model):
    '''Perfil adicional dos usuarios'''

    user = models.OneToOneField(User, unique=True, on_delete=models.CASCADE)
    propriedade = models.ForeignKey('Propriedade', null=True, blank=True, on_delete=models.CASCADE)

    def __unicode__(self):
        return str(self.user)

    class Meta:
        ordering = ['user']


class TipoCentroCusto(models.Model):
	tipo = models.CharField(max_length=100, blank=True, null=True)

	def __unicode__(self):
		return self.tipo

	class Meta:
		verbose_name = 'Tipo de Centro de Custo'
		verbose_name_plural = 'Tipos de Centros de Custo'
		ordering = ['tipo']


class TipoClasse(models.Model):
	tipo = models.CharField(max_length=100, blank=True, null=True)

	def __unicode__(self):
		return self.tipo

	class Meta:
		verbose_name = 'Tipo de Classe'
		verbose_name_plural = 'Tipos de Classes'
		ordering = ['tipo']


class TipoContratacao(models.Model):
	tipo = models.CharField(max_length=100, blank=True, null=True)

	def __unicode__(self):
		return self.tipo

	class Meta:
		verbose_name = 'Tipo de Contratacao'
		verbose_name_plural = 'Tipos de Contratacao'
		ordering = ['tipo']


class TipoEquipamento(models.Model):
	tipo = models.CharField(max_length=100, blank=True, null=True)

	def __unicode__(self):
		return self.tipo

	class Meta:
		verbose_name = 'Tipo de Equipamento'
		verbose_name_plural = 'Tipos de Equipamentos'
		ordering = ['tipo']


class TipoInsumo(models.Model):
	nome = models.CharField(max_length=100)
	tipo = models.CharField('Tipo', max_length=1, choices=TIPO_INSUMO_CHOICES)
	unidade = models.CharField('Unidade', max_length=1, choices=UNIDADE_CHOICES)
	classe = models.ForeignKey(TipoClasse, blank=True, null=True, on_delete=models.CASCADE)
	dosagem_indicada = models.CharField(max_length=100, null=True, blank=True)
	carencia_indicada = models.SmallIntegerField(null=True, blank=True)

	def __unicode__(self):
		return self.nome

	class Meta:
		verbose_name = 'Tipo de Insumo'
		verbose_name_plural = 'Tipos de Insumos'
		ordering = ['nome']


class FilterUserManager(models.Manager):
	def get_queryset_by_user(self, user):
		qs = super(FilterUserManager, self).get_queryset()
		if user.perfilusuario.propriedade is None:
			return qs
		return qs.filter(propriedade=user.perfilusuario.propriedade)


class Propriedade(models.Model):
	nome = models.CharField(max_length=250)
	area = models.FloatField(blank=True, null=True)
	talhoes = models.SmallIntegerField(null=True, blank=True)
	glebas = models.SmallIntegerField(null=True, blank=True)
	geom = models.MultiPolygonField(srid=4326, blank=True, null=True)
	objects = FilterUserManager()

	def __unicode__(self): 
		return self.nome

	@property
	def propriedade(self):
		return unicode(self.id)

	@property
	def nome_propriedade(self):
		return unicode(self.nome)

	class Meta:
		verbose_name = 'Propriedade'
		verbose_name_plural = 'Propriedades'
		ordering = ['nome']


class Responsavel(models.Model):
	'''Pessoas'''

	propriedade = models.ForeignKey(Propriedade, on_delete=models.CASCADE)
	user = models.ForeignKey(User, on_delete=models.CASCADE)
	nome = models.CharField(max_length=255, null=True, blank=True)
	cargo_funcao = models.CharField(max_length=255, null=True, blank=True)
	telefone = models.CharField(max_length=10, null=True, blank=True)

	def __unicode__(self):
		return unicode(self.nome)

	class Meta:
		verbose_name = 'Responsavel'
		verbose_name_plural = 'Responsaveis'



class ClienteFornecedor(models.Model):
	propriedade = models.ForeignKey(Propriedade, on_delete=models.CASCADE)
	tipo = models.CharField('Tipo Cadastro', max_length=1, choices=TIPO_CHOICES)
	tipo_pessoa = models.CharField('Tipo Pessoa', max_length=1, choices=TIPO_PESSOA_CHOICES)
	razao_social = models.CharField(max_length=250, null=True, blank=True)
	nome_fantasia = models.CharField(max_length=250, null=True, blank=True)
	cpf_cnpj = models.CharField(max_length=14, null=True, blank=True)
	nome_contato = models.CharField(max_length=250, null=True, blank=True)
	email = models.CharField(max_length=250, null=True, blank=True)
	telefone = models.CharField(max_length=10, null=True, blank=True)
	cep = models.CharField('CEP', max_length=9, null=True, blank=True)
	logradouro = models.CharField('Logradouro', max_length=200, null=True, blank=True)
	numero = models.CharField('Número', max_length=100, null=True, blank=True)
	complemento = models.CharField('Complemento', max_length=200, null=True, blank=True)
	bairro = models.CharField('Bairro', max_length=100, null=True, blank=True)
	geom = models.PointField('Latitude/Longitude', null=True, blank=True)
	
	objects = FilterUserManager()

	def __unicode__(self): 
		return self.nome_fantasia

	class Meta:
		verbose_name = 'Cliente/Fornecedor'
		verbose_name_plural = 'Clientes/Fornecedores'
		ordering = ['nome_fantasia']


class MaoDeObra(models.Model):
	propriedade = models.ForeignKey(Propriedade, on_delete=models.CASCADE)
	nome = models.CharField(max_length=100)
	matricula = models.CharField(max_length=100)
	funcao = models.CharField(max_length=100, null=True, blank=True)
	regime_contratacao = models.ForeignKey(TipoContratacao, null=True, blank=True, on_delete=models.CASCADE)
	data_admissao = models.DateField(null=True, blank=True)
	carga_horaria = models.SmallIntegerField(blank=True, null=True)
	centro_custo = models.ForeignKey(TipoCentroCusto, null=True, blank=True, on_delete=models.CASCADE)
	salario = models.FloatField(blank=True, null=True)
	encargos_trabalhistas = models.FloatField(blank=True, null=True)
	beneficios = models.FloatField(blank=True, null=True)
	custo_hora = models.FloatField(blank=True, null=True)

	objects = FilterUserManager()

	def __unicode__(self): 
		return self.nome

	class Meta:
		verbose_name = 'MaoDeObra'
		verbose_name_plural = 'MaoDeObra'
		ordering = ['nome','funcao']


class Produto(models.Model):
	propriedade = models.ForeignKey(Propriedade, on_delete=models.CASCADE)
	produto = models.CharField(max_length=100)
	tipo = models.CharField(max_length=100, null=True, blank=True)
	variedade = models.CharField(max_length=100, null=True, blank=True)
	ciclo_viveiro = models.SmallIntegerField(blank=True, null=True)
	ciclo_campo = models.SmallIntegerField(blank=True, null=True)
	celulas_bandeja = models.SmallIntegerField(blank=True, null=True)
	densidade = models.FloatField(blank=True, null=True)
	tipo_plantio = models.CharField('Tipo Plantio', max_length=1, choices=TIPO_PLANTIO_CHOICES, null=True, blank=True)

	objects = FilterUserManager()

	def __unicode__(self): 
		if not self.tipo:
			return unicode(self.produto)
		return unicode(self.produto) + ' ' + unicode(self.tipo)

	class Meta:
		verbose_name = 'Produto'
		verbose_name_plural = 'Produtos'
		ordering = ['produto']


class Equipamento(models.Model):
	propriedade = models.ForeignKey(Propriedade, on_delete=models.CASCADE)
	equipamento = models.ForeignKey(TipoEquipamento, on_delete=models.CASCADE)
	fabricante = models.CharField(max_length=100, null=True, blank=True)
	valor_inicial = models.FloatField(blank=True, null=True)
	depreciacao = models.FloatField(blank=True, null=True)
	manutencao = models.FloatField(blank=True, null=True)
	custo_hora = models.FloatField(blank=True, null=True)
	ano = models.CharField(max_length=4, null=True, blank=True)

	objects = FilterUserManager()

	def __unicode__(self): 
		return unicode(self.equipamento)

	class Meta:
		verbose_name = 'Equipamento'
		verbose_name_plural = 'Equipamentos'
		ordering = ['equipamento']


class Insumo(models.Model):
	propriedade = models.ForeignKey(Propriedade, on_delete=models.CASCADE)
	insumo = models.ForeignKey(TipoInsumo, on_delete=models.CASCADE)
	fabricante = models.CharField(max_length=100, null=True, blank=True)
	data_compra = models.DateField(blank=True, null=True)
	data_validade = models.DateField(blank=True, null=True)
	preco_unidade = models.FloatField(blank=True, null=True)
	quantidade = models.FloatField(blank=True, null=True)
	valor_total = models.FloatField(blank=True, null=True)

	objects = FilterUserManager()

	def __unicode__(self): 
		return unicode(self.insumo)

	class Meta:
		verbose_name = 'insumo'
		verbose_name_plural = 'Insumos'
		ordering = ['insumo']