from django.conf.urls import include, url
from django.views.generic import TemplateView
from django.core.urlresolvers import reverse_lazy
from django.contrib import admin

from . import views

urlpatterns = [

	url(r'^produto_cadastrar/$', views.produto_cadastrar, name='produto_cadastrar'),
	url(r'^produto_listar/$', views.produto_listar, name='produto_listar'),
	url(r'^produto_editar/(?P<id>[-\w]+)/$', views.produto_editar, name='produto_editar'),
	url(r'^produto_remover/(?P<id>[-\w]+)/$', views.produto_remover, name='produto_remover'),

	url(r'^equipamento_cadastrar/$', views.equipamento_cadastrar, name='equipamento_cadastrar'),
	url(r'^equipamento_listar/$', views.equipamento_listar, name='equipamento_listar'),
	url(r'^equipamento_editar/(?P<id>[-\w]+)/$', views.equipamento_editar, name='equipamento_editar'),
	url(r'^equipamento_remover/(?P<id>[-\w]+)/$', views.equipamento_remover, name='equipamento_remover'),

	url(r'^insumo_cadastrar/$', views.insumo_cadastrar, name='insumo_cadastrar'),
	url(r'^insumo_listar/$', views.insumo_listar, name='insumo_listar'),
	url(r'^insumo_editar/(?P<id>[-\w]+)/$', views.insumo_editar, name='insumo_editar'),
	url(r'^insumo_remover/(?P<id>[-\w]+)/$', views.insumo_remover, name='insumo_remover'),

	url(r'^mdo_cadastrar/$', views.mdo_cadastrar, name='mdo_cadastrar'),
	url(r'^mdo_listar/$', views.mdo_listar, name='mdo_listar'),
	url(r'^mdo_editar/(?P<id>[-\w]+)/$', views.mdo_editar, name='mdo_editar'),
	url(r'^mdo_remover/(?P<id>[-\w]+)/$', views.mdo_remover, name='mdo_remover'),

	url(r'^clientefornecedor_cadastrar/$', views.clientefornecedor_cadastrar, name='clientefornecedor_cadastrar'),
	url(r'^clientefornecedor_listar/$', views.clientefornecedor_listar, name='clientefornecedor_listar'),
]
