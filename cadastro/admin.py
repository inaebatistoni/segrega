from django.contrib.gis import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from django import forms

from cadastro.models import (
	Propriedade,
	Responsavel,
	ClienteFornecedor,
	Produto,
	MaoDeObra,
	Equipamento,
	TipoCentroCusto,
	TipoContratacao,
	TipoClasse,
	TipoEquipamento,
	TipoInsumo,
	Insumo,
	PerfilUsuario,
)


class PerfilUsuarioAdmin(admin.StackedInline):
    model = PerfilUsuario
    can_delete = False
    verbose_name_plural = 'Atributos'
    ordering = ['user']

# Define a new User admin
class UserAdmin(UserAdmin):
    inlines = (PerfilUsuarioAdmin, )

# Re-register UserAdmin
admin.site.unregister(User)
admin.site.register(User, UserAdmin)



class PropriedadeAdmin(admin.OSMGeoAdmin):
    list_display = ('id','nome',)
    ordering = ['id']

admin.site.register(Propriedade,PropriedadeAdmin)


class ResponsavelAdmin(admin.OSMGeoAdmin):
    list_display = ('nome',)
    list_filter = ('propriedade',)

admin.site.register(Responsavel,ResponsavelAdmin)


class ClienteFornecedorAdmin(admin.OSMGeoAdmin):
    list_display = ('nome_fantasia', 'tipo', 'tipo_pessoa', 'propriedade')
    list_filter = ('tipo_pessoa', 'propriedade',)
    # form = ClienteFornecerdorForm

admin.site.register(ClienteFornecedor,ClienteFornecedorAdmin,)


class ProdutoAdmin(admin.OSMGeoAdmin):
    list_display = ('produto','tipo', 'celulas_bandeja','densidade', 'propriedade')
    list_filter = ('propriedade',)

admin.site.register(Produto,ProdutoAdmin)


class MaoDeObraAdmin(admin.OSMGeoAdmin):
    list_display = ('nome', 'regime_contratacao', 'custo_hora')
    list_filter = ('propriedade',)

admin.site.register(MaoDeObra,MaoDeObraAdmin)


class TipoCentroCustoAdmin(admin.OSMGeoAdmin):
    list_display = ('tipo',)

admin.site.register(TipoCentroCusto,TipoCentroCustoAdmin)


class TipoContratacaoAdmin(admin.OSMGeoAdmin):
    list_display = ('tipo',)

admin.site.register(TipoContratacao,TipoContratacaoAdmin)


class TipoClasseAdmin(admin.OSMGeoAdmin):
    list_display = ('tipo',)

admin.site.register(TipoClasse,TipoClasseAdmin)


class TipoEquipamentoAdmin(admin.OSMGeoAdmin):
    list_display = ('tipo',)

admin.site.register(TipoEquipamento,TipoEquipamentoAdmin)


class TipoInsumoAdmin(admin.OSMGeoAdmin):
    list_display = ('nome','tipo','classe')

admin.site.register(TipoInsumo,TipoInsumoAdmin)


class EquipamentoAdmin(admin.OSMGeoAdmin):
    list_display = ('equipamento','custo_hora','propriedade')
    list_filter = ('propriedade',)

admin.site.register(Equipamento,EquipamentoAdmin)

class InsumoAdmin(admin.OSMGeoAdmin):
    list_display = ('insumo', 'propriedade')
    list_filter = ('propriedade',)

admin.site.register(Insumo,InsumoAdmin)