var latitude = parseFloat(document.getElementById("latitude").innerHTML);
var longitude = parseFloat(document.getElementById("longitude").innerHTML);

var projection = ol.proj.get('EPSG:900913');

// var fundo = new ol.layer.Tile({source: new ol.source.OSM({})});

var fundo = new ol.layer.Tile({
	source: new ol.source.XYZ({
		// url: 'http://a.tile.openstreetmap.org/{z}/{x}/{y}.png'

		// url: 'http://tile.thunderforest.com/landscape/${z}/${x}/${y}.png'

		// url: 'https://mt1.google.com/vt/lyrs=y&x={x}&y={y}&z={z}'

		url: 'https://server.arcgisonline.com/ArcGIS/rest/services/' + 'World_Imagery/MapServer/tile/{z}/{y}/{x}'

		// url: 'https://server.arcgisonline.com/ArcGIS/rest/services/' + 'World_Topo_Map/MapServer/tile/{z}/{y}/{x}'

		// url: 'https://api.tiles.mapbox.com/v4/mapbox.streets/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiY2hyaXN0aWFub2FuZGVyc29uIiwiYSI6ImNpankzd2Q3NzFrdHJ3ZGtpYnNlOTN0Y3EifQ.pphq3WQuEWdTu4IfCIJW-w'
	})
});

var container = document.getElementById('popup'),
	content = document.getElementById('popup-content'),
	closer = document.getElementById('popup-closer');

var overlay = new ol.Overlay({
    element: container
});

closer.onclick = function() {
    overlay.setPosition(undefined);
    closer.blur();
    return false;
};


var attribution = new ol.control.Attribution({
	collapsible: true
});

var talhoesglebas = new ol.layer.Vector({
	source: new ol.source.Vector({
		url: '../../mapa/talhoesglebas.geojson',
		projection: 'EPSG:900913',
		format: new ol.format.GeoJSON()
	}),

});

var propriedades = new ol.layer.Vector({
	source: new ol.source.Vector({
		url: '../../mapa/propriedades.geojson',
		projection: 'EPSG:900913',
		format: new ol.format.GeoJSON()
	}),

});

var map = new ol.Map({
	controls: ol.control.defaults({attribution: false}).extend([attribution]),

	layers: [
	fundo,
	],
	overlays: [overlay],
	target: 'map',
	view: new ol.View({
		center: ol.proj.transform(
			[longitude, latitude],
			'EPSG:4326', 'EPSG:900913'),
		zoom: 18
	})
});

map.addLayer(propriedades);
map.addLayer(talhoesglebas);



// Ao clicar no mapa chamar função de click
map.on('click', function(e) {
	
	var coordinate = e.coordinate;

	var feature = map.forEachFeatureAtPixel(e.pixel, function(feature, layer) {
		return feature;
	});

	// se achar feature onde clicou
	if(feature !== undefined) {

		if(feature.get('id')) {
			window.console.log(feature)
			var link = "<h4>Talhão/Gleba: " + feature.get('talhao') + "." + feature.get('gleba') + "</h4><p><a href='../ciclo/talhaogleba_detalhe/" + feature.get('id') + "'>Ver Relatório</a></p><p>Cadastrar:<br><a href='../ciclo/preparosolo_cadastrar/?talhao_gleba=" + feature.get('id') + "'>Preparo do Solo</a><br><a href='../ciclo/adubacao_cadastrar/?talhao_gleba=" + feature.get('id') + "'>Adubação</a><br><a href='../ciclo/fitossanidade_cadastrar/?talhao_gleba=" + feature.get('id') + "'>Fitossanidade</a><br><a href='../ciclo/plantio_cadastrar/?talhao_gleba=" + feature.get('id') + "'>Plantio</a><br><a href='../ciclo/colheita_cadastrar/?talhao_gleba=" + feature.get('id') + "'>Colheita</a></p> ";
			content.innerHTML = link;
			overlay.setPosition(coordinate);
			
		}
	} else {
		closer.onclick();
	}
});


