
$(document).ready(function() {


    // $("#lote_colheita").hide();
    // $("#lote_compra").hide();

        var origem = $("#id_origem").val();
        if (origem == 1) {
            $("#lote_colheita").show();
            $("#lote_compra").hide();

        } if (origem == 2) {
            $("#lote_colheita").hide();
            $("#lote_compra").show();

        } else {
            $("#lote_colheita").hide();
            $("#lote_compra").hide();
        }

    $("#id_origem").change(function() {
        var origem = $("#id_origem").val();
        if (origem == 1) {
            $("#lote_colheita").show();
            $("#lote_compra").hide();

        } else {
            $("#lote_colheita").hide();
            $("#lote_compra").show();
        }

    });

    $("#id_lote_colheita").change(function() {
        var lote = $("select#id_lote_colheita option:selected").attr("value");

        var origem = 1;

        $.ajax({
            type: "GET",
            url: "/comercializacao/ajax_lote_venda/" + origem + "/" + lote + "/",
            success: function(data) {
                $("#id_lote").val(data);
            }
        });

    });

    $("#id_lote_compra").change(function() {
        var lote = $("select#id_lote_compra option:selected").attr("value");

        var origem = 2;
        
        $.ajax({
            type: "GET",
            url: "/comercializacao/ajax_lote_venda/" + origem + "/" + lote + "/",
            success: function(data) {
                $("#id_lote").val(data);
            }
        });

    });


});
