# -*- coding: utf-8 -*-

"""Development settings and globals."""

from .base import *

DEBUG = True

ALLOWED_HOSTS = ['127.0.0.1']

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': 'segrega2',
        'USER': 'inaebatistoni',
        'CONN_MAX_AGE': 600,
    },
}
