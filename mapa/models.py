# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.contrib.gis.db import models

from cadastro.models import (
    Propriedade,
    FilterUserManager
    )


class TalhoesGlebas(models.Model):
	id = models.IntegerField(primary_key=True)
	area = models.FloatField(blank=True, null=True)
	talhao = models.CharField(max_length=10, blank=True, null=True)
	gleba = models.IntegerField(blank=True, null=True)
	propriedade = models.ForeignKey(Propriedade, on_delete=models.CASCADE)
	geom = models.MultiPolygonField(srid=4326, blank=True, null=True)
	objects = FilterUserManager()

	def __unicode__(self):
		
		return unicode(self.talhao) + '.' + unicode(self.gleba)

	@property
	def nome_propriedade(self):
		return unicode(self.propriedade.nome)

	class Meta:
		verbose_name = 'TalhoesGlebas'
		verbose_name_plural = 'TalhoesGlebas'
		ordering = ['talhao','propriedade']



