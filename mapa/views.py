# -*- coding: utf-8 -*-

from django.shortcuts import render
from django.template import RequestContext, loader
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.shortcuts import redirect, render, get_object_or_404, render_to_response
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_protect
from vectorformats.Formats import Django, GeoJSON
from django.contrib.auth.models import User

from django.utils import timezone
import datetime

from django.conf import settings

from mapa.models import (
    TalhoesGlebas,
    )

from cadastro.models import (
    PerfilUsuario,
    Propriedade,
    )

@login_required
def index(request):
    '''Pagina inicial do mapa'''

    propriedade=request.user.perfilusuario.propriedade

    context = {
        'propriedade': propriedade,
    }

    return render(request, 'mapa.html', context)


def talhoesglebas(request):
    '''Lista '''

    locais = TalhoesGlebas.objects.get_queryset_by_user(request.user)

    # locais = TalhoesGlebas.objects.all()

    djf = Django.Django(
    	geodjango='geom', 
    	properties=['id', 
    	'nome_propriedade',
    	'talhao', 
    	'gleba',
    	])
    geoj = GeoJSON.GeoJSON()
    s = geoj.encode(djf.decode(locais))

    return HttpResponse(s)


def propriedades(request):
    '''Lista '''

    locais = Propriedade.objects.all()

    djf = Django.Django(
        geodjango='geom', 
        properties=['id', 
        'nome_propriedade',
        'talhoes', 
        'glebas',
        'propriedade'
        ])
    geoj = GeoJSON.GeoJSON()
    s = geoj.encode(djf.decode(locais))

    return HttpResponse(s)

