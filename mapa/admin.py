from django.contrib.gis import admin

from mapa.models import (
	TalhoesGlebas,
)

class TalhoesGlebasAdmin(admin.OSMGeoAdmin):
    list_display = ('id','talhao','gleba','area','propriedade')
    list_filter = ('propriedade','talhao')
    ordering = ['id']

admin.site.register(TalhoesGlebas, TalhoesGlebasAdmin)

