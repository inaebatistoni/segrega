from django.conf.urls import include, url
from django.views.generic import TemplateView

from . import views

urlpatterns = [

    # url(r'^$', TemplateView.as_view(template_name='mapa.html'), name='mapa'),

    url(r'^$', views.index, name='mapa'),


    url(r'^talhoesglebas.geojson$', views.talhoesglebas, name='talhoesglebas'),
    url(r'^propriedades.geojson$', views.propriedades, name='propriedades'),

]
