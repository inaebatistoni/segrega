# -*- coding: utf-8  -*-

from django.forms import inlineformset_factory
from django import forms
from django.contrib.auth.models import User
from django.utils import timezone

from cadastro.models import (
	ClienteFornecedor,
	Produto,
)

from ciclo.models import (
	Colheita,
    )

from comercializacao.models import (
	Venda,
	Compra,
    )

class VendaForm(forms.ModelForm):

	def __init__(self, *args, **kwargs):
		self.request_user = kwargs.pop('request_user')
		super(VendaForm, self).__init__(*args, **kwargs)

		self.fields["lote_colheita"].queryset = Colheita.objects.get_queryset_by_user(self.request_user).order_by('-data_colheita')
		self.fields["lote"].initial = ''

	class Meta:
		model = Venda
		fields = '__all__'
		exclude = ['user','propriedade','status_lote']
		widgets = {
			'data_venda': forms.DateInput(attrs={'class': 'date-picker form-control'}),
			'lote': forms.NumberInput(attrs={'class': 'form-control'}),
			'cliente': forms.Select(attrs={'class': 'form-control'}),
			'origem': forms.Select(attrs={'class': 'form-control'}),
			'lote_colheita': forms.Select(attrs={'class': 'form-control'}),
			'lote_compra': forms.Select(attrs={'class': 'form-control'}),
			'quantidade': forms.NumberInput(attrs={'class': 'form-control'}),
			'preco_unidade': forms.TextInput(attrs={'class': 'form-control '}),
			'valor_total': forms.NumberInput(attrs={'class': 'form-control', 'onfocus':'soma()'}),
			
		}

	def save(self, commit=True):
		instance = super(VendaForm, self).save(commit=False)
		
		instance.user = self.request_user
		instance.data_cadastro = timezone.now()
		instance.propriedade = self.request_user.perfilusuario.propriedade
		instance.status_lote = "1"

		if commit:
			instance.save()
			self.save_m2m()
		return instance


class CompraForm(forms.ModelForm):

	def __init__(self, *args, **kwargs):
		self.request_user = kwargs.pop('request_user')
		super(CompraForm, self).__init__(*args, **kwargs)

		self.fields["lote"].initial = ''

	class Meta:
		model = Compra
		fields = '__all__'
		exclude = ['user','propriedade','status_lote']
		widgets = {
			'data_compra': forms.DateInput(attrs={'class': 'date-picker form-control'}),
			'lote': forms.NumberInput(attrs={'class': 'form-control'}),
			'fornecedor': forms.Select(attrs={'class': 'form-control'}),
			'produto': forms.Select(attrs={'class': 'form-control'}),
			'tipo_unidade': forms.Select(attrs={'class': 'form-control'}),
			'quantidade': forms.NumberInput(attrs={'class': 'form-control'}),
			'preco_unidade': forms.TextInput(attrs={'class': 'form-control '}),
			'valor_total': forms.NumberInput(attrs={'class': 'form-control', 'onfocus':'soma()'}),
			'lote': forms.NumberInput(attrs={'class': 'form-control'}),
			
		}

	def save(self, commit=True):
		instance = super(CompraForm, self).save(commit=False)
		
		instance.user = self.request_user
		instance.data_cadastro = timezone.now()
		instance.propriedade = self.request_user.perfilusuario.propriedade
		instance.status_lote = "1"

		if commit:
			instance.save()
			self.save_m2m()
		return instance

