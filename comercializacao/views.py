# coding: utf-8

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ImproperlyConfigured
from django.core.urlresolvers import reverse_lazy, reverse
from django.forms import models as model_forms, ModelChoiceField
from django.forms.formsets import formset_factory
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.shortcuts import redirect, render, get_object_or_404, render_to_response
from django.contrib.admin.views.decorators import staff_member_required
from django.apps import apps

from django.contrib.auth.models import User

from django.utils import timezone
import datetime

from django.conf import settings

from cadastro.models import (
	PerfilUsuario,
	Propriedade,
	ClienteFornecedor,
	)

from comercializacao.models import (
	Venda,
	Compra,
    )

from .forms import (
	VendaForm,
	CompraForm,
	)


@login_required
def venda_cadastrar(request):

	form = VendaForm(
		data=request.POST or None,
		request_user=request.user,
	)

	if request.POST:
		
		if form.is_valid():

			f = form.save()

			messages.success(request, u'Venda cadastrada com sucesso')
			return HttpResponseRedirect(reverse_lazy('venda_listar'))

	context = {
		'form': form,
	}

	return render(request, 'comercializacao/venda_cadastrar.html', context)

@login_required
def ajax_lote_venda(request, origem, lote):
    '''Carrega ultimo lote de venda com base no lote do colheita '''

    if origem == "1":
    	lote = Venda.objects.filter(lote_colheita_id=int(lote)).order_by('-lote').first()
    else:
    	lote = Venda.objects.filter(lote_compra_id=int(lote)).order_by('-lote').first()

    if lote is None:
    	lote = 1
    else:
    	lote = lote.lote + 1

    return HttpResponse(lote)


@login_required
def venda_listar(request):

	lote = Venda.objects.get_queryset_by_user(request.user).order_by('-data_venda')

	context = {
		'lote': lote,
	}

	return render(request, 'comercializacao/venda_listar.html', context)


@login_required
def venda_editar(request, lote):

	lote = Venda.objects.get(pk=int(lote))

	form = VendaForm(request.POST or None, instance=lote, request_user=request.user)

	if request.method == 'POST':
		
		if form.is_valid():

			f = form.save()

			messages.success(request, u'Venda editado com sucesso')
			return HttpResponseRedirect(reverse_lazy('venda_listar'))

	context = {
		'form': form,
	}

	return render(request, 'comercializacao/venda_editar.html', context)


@login_required
def venda_remover(request, lote):

	lote = Venda.objects.get(pk=int(lote))

	lote.delete()

	messages.success(request, u'Venda removida com sucesso')
	return HttpResponseRedirect(reverse_lazy('venda_listar'))	


@login_required
def venda_fechar_abrir(request, lote):

	lote = Venda.objects.get(pk=int(lote))
	
	if lote.status_lote == '1':
		lote.status_lote = '2'
		lote.save()

	elif lote.status_lote == '2':
		lote.status_lote = '1'
		lote.save()

	messages.success(request, u'Lote fechado/aberto com sucesso')
	return HttpResponseRedirect(reverse_lazy('venda_listar'))




@login_required
def compra_cadastrar(request):

	form = CompraForm(
		data=request.POST or None,
		request_user=request.user,
	)

	lote = Compra.objects.get_queryset_by_user(request.user).order_by('-lote').first()

	if lote is None:
		lote = 1
	else:
		lote = lote.lote + 1


	if request.POST:
		
		if form.is_valid():

			f = form.save()

			messages.success(request, u'Compra cadastrada com sucesso')
			return HttpResponseRedirect(reverse_lazy('compra_listar'))

	context = {
		'form': form,
		'lote': lote,
	}

	return render(request, 'comercializacao/compra_cadastrar.html', context)



@login_required
def compra_listar(request):

	lote = Compra.objects.get_queryset_by_user(request.user).order_by('-data_compra')

	context = {
		'lote': lote,
	}

	return render(request, 'comercializacao/compra_listar.html', context)


@login_required
def compra_editar(request, lote):

	lote = Compra.objects.get(pk=int(lote))

	form = CompraForm(request.POST or None, instance=lote, request_user=request.user)

	if request.method == 'POST':
		
		if form.is_valid():

			f = form.save()

			messages.success(request, u'Compra editado com sucesso')
			return HttpResponseRedirect(reverse_lazy('compra_listar'))

	context = {
		'form': form,
	}

	return render(request, 'comercializacao/compra_editar.html', context)


@login_required
def compra_remover(request, lote):

	lote = Compra.objects.get(pk=int(lote))

	lote.delete()

	messages.success(request, u'Compra removida com sucesso')
	return HttpResponseRedirect(reverse_lazy('compra_listar'))	


@login_required
def compra_fechar_abrir(request, lote):

	lote = Compra.objects.get(pk=int(lote))
	
	if lote.status_lote == '1':
		lote.status_lote = '2'
		lote.save()

	elif lote.status_lote == '2':
		lote.status_lote = '1'
		lote.save()

	messages.success(request, u'Lote fechado/aberto com sucesso')
	return HttpResponseRedirect(reverse_lazy('compra_listar'))


