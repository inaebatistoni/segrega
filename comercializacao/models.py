# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.gis.db import models
from django.contrib.auth.models import User
from django import utils
from django.utils import timezone

from cadastro.models import (
    Equipamento,
    Propriedade,
    ClienteFornecedor,
    Produto,
    FilterUserManager,
    )

from ciclo.models import (
	Colheita,
    )

ORIGEM_CHOICES = (
    ('1', 'Própria'),
    ('2', 'Terceiros'),
)

TIPO_UNIDADE_CHOICES = (
    ('1', 'Maço(s)'),
    ('2', 'Unidade(s)'),
    ('3', 'Caixa(s)'),
    ('4', 'Kg'),
    ('5', 'Bandeja(s)'),
    ('6', 'Dúzia(s)'),
    ('7', 'Cento(s)'),
)

STATUS_LOTE_CHOICES = (
    ('1', 'Aberto'),
    ('2', 'Encerrado'),
)

class Compra(models.Model):
	'''Compra'''

	user = models.ForeignKey(User, on_delete=models.CASCADE)
	propriedade = models.ForeignKey(Propriedade, null=True, blank=True, on_delete=models.CASCADE)
	lote = models.IntegerField()
	data_compra = models.DateField()
	fornecedor = models.ForeignKey(ClienteFornecedor, null=True, blank=True, on_delete=models.CASCADE)
	produto = models.ForeignKey(Produto, on_delete=models.CASCADE)
	quantidade = models.FloatField()
	tipo_unidade = models.CharField('Unidade', max_length=1, choices=TIPO_UNIDADE_CHOICES)
	preco_unidade = models.FloatField()
	valor_total = models.FloatField()
	descarte = models.FloatField(blank=True, null=True)
	status_lote = models.CharField('Status do Lote', max_length=1, choices=STATUS_LOTE_CHOICES)
	data_cadastro = models.DateTimeField(blank=True, null=True)

	objects = FilterUserManager()

	def __unicode__(self):
		return unicode(self.lote) + ' / ' + unicode(self.fornecedor) +' / ' + unicode(self.produto) + ' / ' + unicode(self.data_compra)

	class Meta:
		verbose_name = 'Compra'
		verbose_name_plural = 'Compras'


class Venda(models.Model):
	'''Venda'''

	user = models.ForeignKey(User, on_delete=models.CASCADE)
	propriedade = models.ForeignKey(Propriedade, null=True, blank=True, on_delete=models.CASCADE)
	lote = models.IntegerField()
	data_venda = models.DateField()
	cliente = models.ForeignKey(ClienteFornecedor, on_delete=models.CASCADE)
	origem = models.CharField('Origem', max_length=1, choices=ORIGEM_CHOICES)
	lote_colheita = models.ForeignKey(Colheita, null=True, blank=True, on_delete=models.CASCADE)
	lote_compra = models.ForeignKey(Compra, null=True, blank=True, on_delete=models.CASCADE)
	quantidade = models.FloatField()
	preco_unidade = models.FloatField()
	valor_total = models.FloatField()
	status_lote = models.CharField('Status do Lote', max_length=1, choices=STATUS_LOTE_CHOICES)
	data_cadastro = models.DateTimeField(blank=True, null=True)
	
	objects = FilterUserManager()

	def __unicode__(self):
		return unicode(self.id)

	class Meta:
		verbose_name = 'Venda'
		verbose_name_plural = 'Vendas'