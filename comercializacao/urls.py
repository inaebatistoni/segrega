from django.conf.urls import include, url
from django.views.generic import TemplateView
from django.core.urlresolvers import reverse_lazy
from django.contrib import admin

from . import views

urlpatterns = [

	url(r'^venda_cadastrar/$', views.venda_cadastrar, name='venda_cadastrar'),
	url(r'^venda_listar/$', views.venda_listar, name='venda_listar'),
	url(r'^venda_editar/(?P<lote>[-\w]+)/$', views.venda_editar, name='venda_editar'),
	url(r'^venda_remover/(?P<lote>[-\w]+)/$', views.venda_remover, name='venda_remover'),
	url(r'^venda_fechar_abrir/(?P<lote>[-\w]+)/$',  views.venda_fechar_abrir, name='venda_fechar_abrir'),

	url(r'^ajax_lote_venda/(?P<origem>[\w]+)/(?P<lote>[-\w]+)/$', views.ajax_lote_venda, name='ajax_lote_venda'),

	url(r'^compra_cadastrar/$', views.compra_cadastrar, name='compra_cadastrar'),
	url(r'^compra_listar/$', views.compra_listar, name='compra_listar'),
	url(r'^compra_editar/(?P<lote>[-\w]+)/$', views.compra_editar, name='compra_editar'),
	url(r'^compra_remover/(?P<lote>[-\w]+)/$', views.compra_remover, name='compra_remover'),
	url(r'^compra_fechar_abrir/(?P<lote>[-\w]+)/$',  views.compra_fechar_abrir, name='compra_fechar_abrir'),
]
