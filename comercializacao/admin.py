from django.contrib.gis import admin

from comercializacao.models import (
	Compra,
	Venda,
)

class CompraAdmin(admin.OSMGeoAdmin):
    list_display = ('fornecedor','data_compra','quantidade','valor_total','propriedade')

admin.site.register(Compra,CompraAdmin)


class VendaAdmin(admin.OSMGeoAdmin):
    list_display = ('cliente','data_venda','quantidade','valor_total','propriedade')

admin.site.register(Venda,VendaAdmin)

