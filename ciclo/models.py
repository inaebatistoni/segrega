# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.gis.db import models
from django.contrib.auth.models import User
from django import utils
from django.utils import timezone

from cadastro.models import (
    Equipamento,
    Propriedade,
    Insumo,
    Produto,
    MaoDeObra,
    FilterUserManager,
    )

from mapa.models import (
    TalhoesGlebas,
    )

ORIGEM_CHOICES = (
    ('1', 'Viveiro'),
    ('2', 'Compra'),
    ('3', 'Colheita'),
    ('4', 'Não Identificado'),
)

TIPO_UNIDADE_CHOICES = (
    ('1', 'Maço(s)'),
    ('2', 'Unidade(s)'),
    ('3', 'Caixa(s)'),
    ('4', 'Kg'),
    ('5', 'Bandeja(s)'),
    ('6', 'Dúzia(s)'),
    ('7', 'Cento(s)'),
)

STATUS_LOTE_CHOICES = (
    ('1', 'Aberto'),
    ('2', 'Encerrado'),
)

TIPO_PLANTIO_CHOICES = (
    ('1', 'Muda'),
    ('2', 'Semente'),
)


class TipoPreparo(models.Model):
	tipo = models.CharField(max_length=100, blank=True, null=True)

	def __unicode__(self):
		return self.tipo

	class Meta:
		verbose_name = 'Tipo de Preparo'
		verbose_name_plural = 'Tipos de Preparo'
		ordering = ['tipo']


class TipoAdubacao(models.Model):
	tipo = models.CharField(max_length=100, blank=True, null=True)

	def __unicode__(self):
		return self.tipo

	class Meta:
		verbose_name = 'Tipo de Adubação'
		verbose_name_plural = 'Tipos de Adubação'
		ordering = ['tipo']


class PreparoSolo(models.Model):
	'''Preparo do Solo'''

	id = models.AutoField(primary_key=True)
	lote = models.IntegerField()
	user = models.ForeignKey(User, on_delete=models.CASCADE)
	data_preparo = models.DateField()
	propriedade = models.ForeignKey(Propriedade, null=True, blank=True, on_delete=models.CASCADE)
	talhao_gleba = models.ManyToManyField(TalhoesGlebas)
	tipo_preparo = models.ForeignKey(TipoPreparo, on_delete=models.CASCADE)
	equipamento = models.ForeignKey(Equipamento, null=True, blank=True, on_delete=models.CASCADE)
	insumo = models.ForeignKey(Insumo, null=True, blank=True, on_delete=models.CASCADE)
	quantidade = models.FloatField(blank=True, null=True)
	horas_maquina = models.FloatField(blank=True, null=True)
	data_cadastro = models.DateTimeField(blank=True, null=True)

	objects = FilterUserManager()

	def __unicode__(self):
		return unicode(self.id)

	class Meta:
		verbose_name = 'Preparo do Solo'
		verbose_name_plural = 'Preparação do Solo'


class EquipamentoPreparo(models.Model):
	'''Equipamento utilizado - Preparo do Solo'''

	preparo = models.ForeignKey('PreparoSolo', blank=True, null=True, on_delete=models.CASCADE)
	equipamento = models.ForeignKey(Equipamento, blank=True, null=True, on_delete=models.CASCADE)
	horas_maquina = models.SmallIntegerField(blank=True, null=True)

	def __unicode__(self):
		return unicode(self.preparo)


class MdoPreparo(models.Model):
	'''Mão de Obra alocada - Preparo do Solo'''

	preparo = models.ForeignKey('PreparoSolo', blank=True, null=True, related_name='preparo', on_delete=models.CASCADE)
	nome = models.ForeignKey(MaoDeObra, blank=True, null=True, on_delete=models.CASCADE)
	horas = models.SmallIntegerField(blank=True, null=True)

	def __unicode__(self):
		return unicode(self.id)


class Adubacao(models.Model):
	'''Adubação'''

	id = models.AutoField(primary_key=True)
	lote = models.IntegerField()
	user = models.ForeignKey(User, on_delete=models.CASCADE)
	data_adubacao = models.DateField()
	propriedade = models.ForeignKey(Propriedade, null=True, blank=True, on_delete=models.CASCADE)
	talhao_gleba = models.ManyToManyField(TalhoesGlebas)
	tipo_adubacao = models.ForeignKey(TipoAdubacao, on_delete=models.CASCADE)
	equipamento = models.ForeignKey(Equipamento, null=True, blank=True, on_delete=models.CASCADE)
	insumo = models.ForeignKey(Insumo, on_delete=models.CASCADE)
	quantidade = models.FloatField()
	horas_maquina = models.FloatField(blank=True, null=True)
	data_cadastro = models.DateTimeField(blank=True, null=True)

	objects = FilterUserManager()

	def __unicode__(self):
		return unicode(self.id)

	class Meta:
		verbose_name = 'Adubação'
		verbose_name_plural = 'Adubações'


class MdoAdubacao(models.Model):
	'''Mão de Obra alocada - Adubação'''

	adubacao = models.ForeignKey('Adubacao', blank=True, null=True, related_name='adubacao', on_delete=models.CASCADE)
	nome = models.ForeignKey(MaoDeObra, blank=True, null=True, on_delete=models.CASCADE)
	horas = models.SmallIntegerField(blank=True, null=True)

	def __unicode__(self):
		return unicode(self.adubacao)


class Fitossanidade(models.Model):
	'''Fitossanidade'''

	id = models.AutoField(primary_key=True)
	lote = models.IntegerField()
	user = models.ForeignKey(User, on_delete=models.CASCADE)
	data_fito = models.DateField()
	propriedade = models.ForeignKey(Propriedade, null=True, blank=True, on_delete=models.CASCADE)
	talhao_gleba = models.ManyToManyField(TalhoesGlebas)
	praga_doenca = models.CharField(max_length=250, null=True, blank=True)
	equipamento = models.ForeignKey(Equipamento, null=True, blank=True, on_delete=models.CASCADE)
	insumo = models.ForeignKey(Insumo, on_delete=models.CASCADE)
	quantidade = models.FloatField()
	dosagem = models.FloatField(blank=True, null=True)
	horas_maquina = models.FloatField(blank=True, null=True)
	data_cadastro = models.DateTimeField(blank=True, null=True)

	objects = FilterUserManager()

	def __unicode__(self):
		return unicode(self.id)

	class Meta:
		verbose_name = 'Fitossanidade'
		verbose_name_plural = 'Fitossanidade'


class MdoFitossanidade(models.Model):
	'''Mão de Obra alocada - Fitossanidade'''

	fitossanidade = models.ForeignKey('Fitossanidade', blank=True, null=True, related_name='fitossanidade', on_delete=models.CASCADE)
	nome = models.ForeignKey(MaoDeObra, blank=True, null=True, on_delete=models.CASCADE)
	horas = models.SmallIntegerField(blank=True, null=True)

	objects = FilterUserManager()

	def __unicode__(self):
		return unicode(self.fitossanidade)


class Semeio(models.Model):
	'''Semeio'''

	id = models.AutoField(primary_key=True)
	lote = models.IntegerField()
	user = models.ForeignKey(User, on_delete=models.CASCADE)
	data_semeio = models.DateField()
	propriedade = models.ForeignKey(Propriedade, null=True, blank=True, on_delete=models.CASCADE)
	viveiro = models.CharField(blank=True, null=True, max_length=255)
	produto = models.ForeignKey(Produto, on_delete=models.CASCADE)
	n_bandejas = models.SmallIntegerField(blank=True, null=True)
	unidades = models.IntegerField()
	restantes = models.IntegerField(blank=True, null=True)
	status_lote = models.CharField('Status do Lote', max_length=1, choices=STATUS_LOTE_CHOICES)
	data_cadastro = models.DateTimeField(blank=True, null=True)

	objects = FilterUserManager()

	@property
	def unidades_plantadas(self):
		return sum(self.plantio_set.values_list('unidades', flat=True))

	# @property
	# def unidades_restantes(self):
	# 	return unidades - unidades_plantadas

	def __unicode__(self):
		return unicode(self.lote) + ' / ' + unicode(self.produto) + ' / ' + unicode(self.data_semeio)

	class Meta:
		verbose_name = 'Semeio'
		verbose_name_plural = 'Semeios'


class MdoSemeio(models.Model):
	'''Mão de Obra alocada - Semeio'''

	semeio = models.ForeignKey('Semeio', blank=True, null=True, related_name='semeio', on_delete=models.CASCADE)
	nome = models.ForeignKey(MaoDeObra, blank=True, null=True, on_delete=models.CASCADE)
	horas = models.SmallIntegerField(blank=True, null=True)

	def __unicode__(self):
		return unicode(self.semeio)


class Plantio(models.Model):
	'''Plantio'''

	id = models.AutoField(primary_key=True)
	lote = models.IntegerField()
	user = models.ForeignKey(User, on_delete=models.CASCADE)
	data_plantio = models.DateField()
	propriedade = models.ForeignKey(Propriedade, null=True, blank=True, on_delete=models.CASCADE)
	talhao_gleba = models.ManyToManyField(TalhoesGlebas)
	produto = models.ForeignKey(Produto, on_delete=models.CASCADE)
	origem = models.CharField('Origem', max_length=1, choices=ORIGEM_CHOICES)
	lote_semeio = models.ForeignKey(Semeio, null=True, blank=True, on_delete=models.CASCADE)
	tipo_plantio = models.CharField('Tipo de unidade', max_length=1, choices=TIPO_PLANTIO_CHOICES)
	unidades = models.IntegerField()
	restantes = models.IntegerField(blank=True, null=True)
	status_lote = models.CharField('Status do Lote', max_length=1, choices=STATUS_LOTE_CHOICES)
	data_cadastro = models.DateTimeField(blank=True, null=True)

	objects = FilterUserManager()

	@property
	def unidades_colhidas(self):
		return sum(self.colheita_set.values_list('unidades', flat=True))

	def __unicode__(self):
		return unicode(self.lote) + ' / ' + unicode(self.produto) + ' / ' + unicode(self.data_plantio)

	class Meta:
		verbose_name = 'Plantio'
		verbose_name_plural = 'Plantios'


class MdoPlantio(models.Model):
	'''Mão de Obra alocada - Plantio'''

	plantio = models.ForeignKey('Plantio', blank=True, null=True, related_name='plantio', on_delete=models.CASCADE)
	nome = models.ForeignKey(MaoDeObra, blank=True, null=True, on_delete=models.CASCADE)
	horas = models.SmallIntegerField(blank=True, null=True)

	def __unicode__(self):
		return unicode(self.plantio)


class Colheita(models.Model):
	'''Colheita'''

	id = models.AutoField(primary_key=True)
	lote = models.IntegerField()
	user = models.ForeignKey(User, on_delete=models.CASCADE)
	data_colheita = models.DateField()
	propriedade = models.ForeignKey(Propriedade, null=True, blank=True, on_delete=models.CASCADE)
	lote_plantio = models.ForeignKey(Plantio, on_delete=models.CASCADE)
	unidades = models.IntegerField()
	tipo_unidade = models.CharField('Tipo de unidade', max_length=1, choices=TIPO_UNIDADE_CHOICES)
	sobra = models.FloatField(blank=True, null=True)
	status_lote = models.CharField('Status do Lote', max_length=1, choices=STATUS_LOTE_CHOICES)
	data_cadastro = models.DateTimeField(blank=True, null=True)

	objects = FilterUserManager()

	def __unicode__(self):
		return unicode(self.lote_plantio.lote) + '.' + unicode(self.lote) + ' / ' + unicode(self.lote_plantio.produto) + ' / ' + unicode(self.data_colheita)

	class Meta:
		verbose_name = 'Colheita'
		verbose_name_plural = 'Colheitas'


class MdoColheita(models.Model):
	'''Mão de Obra alocada - Colheita'''

	colheita = models.ForeignKey('Colheita', blank=True, null=True, related_name='colheita', on_delete=models.CASCADE)
	nome = models.ForeignKey(MaoDeObra, blank=True, null=True, on_delete=models.CASCADE)
	horas = models.SmallIntegerField(blank=True, null=True)

	def __unicode__(self):
		return unicode(self.colheita)
