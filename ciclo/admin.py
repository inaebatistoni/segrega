
from django.contrib.gis import admin

from ciclo.models import (
	TipoAdubacao,
	TipoPreparo,
	Adubacao,
	PreparoSolo,
	Fitossanidade,
	Semeio,
	Plantio,
	Colheita,
)


class TipoPreparoAdmin(admin.OSMGeoAdmin):
    list_display = ('tipo',)

admin.site.register(TipoPreparo,TipoPreparoAdmin)


class PreparoSoloAdmin(admin.OSMGeoAdmin):
    list_display = ('id','lote','tipo_preparo','data_preparo',)
    list_filter = ('talhao_gleba','propriedade')

admin.site.register(PreparoSolo,PreparoSoloAdmin)


class TipoAdubacaoAdmin(admin.OSMGeoAdmin):
    list_display = ('tipo',)

admin.site.register(TipoAdubacao,TipoAdubacaoAdmin)


class AdubacaoAdmin(admin.OSMGeoAdmin):
    list_display = ('id','lote','tipo_adubacao','data_adubacao','propriedade')
    list_filter = ('talhao_gleba','propriedade')

admin.site.register(Adubacao,AdubacaoAdmin)


class FitossanidadeAdmin(admin.OSMGeoAdmin):
    list_display = ('id','lote','praga_doenca','data_fito','propriedade')
    list_filter = ('talhao_gleba','propriedade')

admin.site.register(Fitossanidade,FitossanidadeAdmin)


class SemeioAdmin(admin.OSMGeoAdmin):
    list_display = ('lote','produto','data_semeio','status_lote')
    list_filter = ('status_lote','produto','propriedade')


admin.site.register(Semeio,SemeioAdmin)


class PlantioAdmin(admin.OSMGeoAdmin):
    list_display = ('lote','data_plantio','status_lote','lote_semeio', 'produto')
    list_filter = ('status_lote','produto','talhao_gleba','propriedade')


admin.site.register(Plantio,PlantioAdmin)


class ColheitaAdmin(admin.OSMGeoAdmin):
    list_display = ('lote','data_colheita','status_lote','lote_plantio')
    list_filter = ('status_lote','propriedade')

admin.site.register(Colheita,ColheitaAdmin)