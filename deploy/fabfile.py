# coding: utf-8
import os
import sys

from ConfigParser import ConfigParser

from fabric.api import cd
from fabric.contrib import django
from fabric.contrib.files import exists, upload_template
from fabric.operations import sudo
from tasks_manager.generic_fabfile import generic_fabfile
from tasks_manager.tasks import *
from tasks_manager.utils import _virtenvsudo, _reload_supervisorctl

from django.conf import settings

# Abre o Arquivo settings.ini com o ConfigParser
ini = ConfigParser()
ini.readfp(open('settings.ini'))

# Lê o nome do projeto
PROJECT_NAME = ini.get('DEPLOY', 'project_name')
try:
    PROJECT_DIR = ini.get('DEPLOY', 'project_dir')
except:
    PROJECT_DIR = False

# Lê qual settings usar na produção
PROD_SETTINGS = ini.get('APP', 'settings_file')

sys.path.append('..')


BASE_DIR = os.path.dirname(os.path.abspath(__file__))
django.settings_module(PROD_SETTINGS)


@task
def sgag():
    # chama o fabfile genérico
    # reescrever aqui caso seja necessário

    # env.branch = 'deploy'
    generic_fabfile(env, settings, ini, PROJECT_NAME, PROJECT_DIR, PROD_SETTINGS)()
    env.django_project_root = env.code_root



